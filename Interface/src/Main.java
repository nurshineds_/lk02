/*
 * Jangan ubah kode didalam kelas Main ini
 * Lakukan modifikasi kode pada kelas pertambahan dan pengurangan
 * Dalam konteks kalkulator, pada kondisi apa interface akan lebih cocok digunakan daripada abstract class?
 * Dalam konteks kalkulator, kondisi interface akan lebih cocok digunakan ketika suatu kelas interface memiliki beberapa method
 * yang seluruh methodnya harus diimplementasikan pada kelas di bawahnya. Misalnya, jika sebuah kelas interface memiliki method
 * perhitungan masing-masing seperti jumlah, kurang, bagi, dan kali, kemudian kelas-kelas kalkulator dibawahnya harus memiliki keempat
 * method tersebut maka penggunaan interface lebih cocok dibandingkan abstract class.
 */

import java.util.*;

public class Main {   
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);

        System.out.print("Masukan operan 1: ");
        double operan1 = scanner.nextDouble();

        System.out.print("Masukan operan 2: ");
        double operan2 = scanner.nextDouble();

        System.out.print("Masukan operator (+ atau -): ");
        String operator = scanner.next();
        scanner.close();

        Kalkulator kalkulator;
        if (operator.equals("+")) {
            kalkulator = new Pertambahan();
        } else if (operator.equals("-")) {
            kalkulator = new Pengurangan();
        } else {
            System.out.println("Operator tidak valid!");
            return;
        }

        double result = kalkulator.hitung(operan1, operan2);
        System.out.println("Result: " + result);
    }
}